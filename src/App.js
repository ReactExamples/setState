import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component{
  constructor(props){//es6 initialState
    super(props);
    this.state = {name: ''};
    this.handleChange = this.handleChange.bind(this);
  }
  render() {
    return (  
      <div className='col-lg-12'>
        <br/>
        <label>Tu nombre es: {this.state.name}</label>
        <br/>
        <input type='text' placeholder='nombre' className='form-control' onChange={this.handleChange}/>
      </div>
    );
  }

  handleChange(e){
    this.setState({name: e.target.value});
  }

}

export default App;
